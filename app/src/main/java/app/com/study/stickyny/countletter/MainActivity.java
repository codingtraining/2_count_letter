package app.com.study.stickyny.countletter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText inputEdit = (EditText) findViewById(R.id.input_edit);
        Button completeBtn = (Button) findViewById(R.id.complete_btn);
        final TextView outputText = (TextView) findViewById(R.id.output_text);

        completeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                outputText.setText(print(inputEdit.getText().toString()));
            }
        });
    }

    private String print(String inputStr){
        return getString(R.string.output_msg, inputStr.length());
    }
}
